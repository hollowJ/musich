import os
def walkMusic(dir):
    musicFiles=set()
    for root,dirs,files in os.walk(dir):
        processDir(root,dirs,files,musicFiles)
    return musicFiles;

def processDir(dirpath,dirnames,filenames,musicFiles):
    # print(dirpath)
    # print(dirnames)
    # print(filenames)
    for musicFile in filenames:
        if musicFile.endswith(".mp3"):
            absolutePath=os.path.join(dirpath, musicFile)
            musicFiles.add(absolutePath)
