import sqlalchemy
from sqlalchemy.orm import sessionmaker

from datamodel.music import Music

DB_CONNECTION_STRING = "sqlite:///music.db"
DB_Session=None
engine = sqlalchemy.create_engine(DB_CONNECTION_STRING, echo=True)
DB_Session = sessionmaker(bind=engine)
def getSession():
    session = DB_Session()
    return session
# session=getSession()
# music = Music(title="胆小鬼", artist="夏婉安", album="谁说电音没感情", duration=251.81718275449393,
#                               size=10165899, src="F:/cloudMusic\\夏婉安 - 胆小鬼.mp3")
# music1 = Music(title="胆小鬼", artist="夏婉安", album="谁说电音没感情", duration=251.81718275449393,
#                               size=10165899, src="F:/cloudMusic\\夏婉安 - 胆小鬼.mp3")
# music2 = Music(title="胆小鬼", artist="夏婉安", album="谁说电音没感情", duration=251.81718275449393,
#                               size=10165899, src="F:/cloudMusic\\夏婉安 - 胆小鬼.mp3")
# session.add(music)
# session.add(music1)
# session.add(music2)
# session.commit()
# session.close()