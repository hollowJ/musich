from PyQt5.QtCore import QUrl, pyqtSignal, QObject
from PyQt5.QtMultimedia import QMediaPlayer, QMediaContent, QMediaPlaylist
from sqlalchemy import asc
from tinytag import TinyTag

from datamodel.music import Music
from util import FileUtil, DbUtil


class MusicBox(QObject):
    currentMusicIndexChanged = pyqtSignal(int)
    currentMusicInfoChanged = pyqtSignal(Music)
    currentMusicDurationChanged = pyqtSignal(int)
    playerStateChanged = pyqtSignal(int)
    playerVolumnChanged = pyqtSignal(int)
    playerBackModeChanged=pyqtSignal(int)
    def __init__(self):
        super(MusicBox, self).__init__()
        self.initComponent()
        self.musicMap = {}
        self.initMusicList()

    def updateMusicList(self, dir):
        newMusicFiles = FileUtil.walkMusic(dir)
        session = DbUtil.getSession()
        for musicFile in newMusicFiles:
            if musicFile not in self.musicMap:
                tag = TinyTag.get(musicFile, image=True)
                music = Music(title=tag.title, artist=tag.artist, album=tag.album, duration=tag.duration,
                              size=tag.filesize, src=musicFile)
                session.add(music)
        session.commit()
        session.close()

    def play(self, index):
        self.mediaList.setCurrentIndex(index)
        self.media.play()
        print("正在播放：" + self.musicList[index].title)

    def initMusicList(self):
        session = DbUtil.getSession()
        self.musicList = session.query(Music).order_by(asc(Music.title)).all()
        for music in self.musicList:
            self.mediaList.addMedia(QMediaContent(QUrl.fromLocalFile(music.src)))

    def initComponent(self):
        self.media = QMediaPlayer()
        self.mediaList = QMediaPlaylist()
        self.media.setPlaylist(self.mediaList)
        self.mediaList.currentIndexChanged.connect(lambda i: self.currentMusicIndexChanged.emit(i))
        self.media.stateChanged.connect(lambda i: self.playerStateChanged.emit(i))
        self.musicList = []
        self.mediaList.currentIndexChanged.connect(lambda i: self.currentMusicInfoChanged.emit(self.musicList[i]))
        self.media.positionChanged.connect(lambda i: self.currentMusicDurationChanged.emit(i))
        self.media.volumeChanged.connect(lambda i: self.playerVolumnChanged.emit(i))
        self.mediaList.playbackModeChanged.connect(lambda  i:self.playerBackModeChanged.emit(i))

    def next(self):
        self.mediaList.next()

    def previous(self):
        self.mediaList.previous()

    def pause(self):
        self.media.pause()

    def playOrPause(self):
        if self.media.state() == QMediaPlayer.PlayingState:
            self.pause()
        elif self.media.state() == QMediaPlayer.PausedState:
            self.media.play()

    def onPlayerVolumnChange(self, volumn):
        self.media.setVolume(volumn)

    def getPlayerVolumn(self):
        return self.media.volume()
    def getPlayerLoop(self):
        return self.mediaList.playbackMode()
    def setPlayBackMode(self,playBackMode):
        self.mediaList.setPlaybackMode(playBackMode)
