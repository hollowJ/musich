from sqlalchemy import column, String, Integer, Column

from datamodel import Base


class Music(Base):
    __tablename__ = "music"
    id = Column("id", Integer, primary_key=True, autoincrement=True)
    title = Column("title", String)
    artist = Column("artist", String)
    album = Column("album", String)
    duration = Column("duration", String)
    size = Column("size", String)
    src = Column("src", String)
