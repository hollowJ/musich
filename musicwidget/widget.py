from PyQt5.QtCore import Qt
from PyQt5.QtGui import QStandardItemModel, QStandardItem
from PyQt5.QtWidgets import QWidget, QGridLayout, QLabel, QHBoxLayout, QVBoxLayout, QPushButton, QFileDialog, \
    QTableWidget, QTableView, QHeaderView, QAbstractItemView

from datamodel.musicbox import MusicBox


class MusicListWidget(QWidget):
    def __init__(self, musicBox):
        super(MusicListWidget, self).__init__()
        self.musicBox = musicBox
        self.initUI()
        self.initEvent()

    def initUI(self):
        self.btn_selectDir = QPushButton("指定文件夹")
        self.btn_playAll = QPushButton("播放全部")
        self.btn_localQuery = QPushButton("本地搜索")
        vBoxLayout = QVBoxLayout()
        hBoxLayout = QHBoxLayout()
        hBoxLayout.addWidget(self.btn_selectDir)
        hBoxLayout.addWidget(self.btn_playAll)
        hBoxLayout.addWidget(self.btn_localQuery)
        vBoxLayout.addLayout(hBoxLayout)
        self.tableView = QTableView()
        self.model = QStandardItemModel()
        self.tableView.setModel(self.model)
        self.tableView.horizontalHeader().setStretchLastSection(True)
        self.tableView.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.tableView.setSelectionMode(QAbstractItemView.SingleSelection)
        self.tableView.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.tableView.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.model.setHorizontalHeaderLabels(["标题", "歌手", "专辑", "时常", "大小(MB)"])

        for rowNum in range(0, len(self.musicBox.musicList)):
            titleItem = QStandardItem(self.musicBox.musicList[rowNum].title)
            titleItem.setTextAlignment(Qt.AlignCenter)
            self.model.setItem(rowNum, 0, titleItem)
            artistItem = QStandardItem(self.musicBox.musicList[rowNum].artist)
            self.model.setItem(rowNum, 1, artistItem)
            artistItem.setTextAlignment(Qt.AlignCenter)
            albumItem = QStandardItem(self.musicBox.musicList[rowNum].album)
            self.model.setItem(rowNum, 2, albumItem)
            albumItem.setTextAlignment(Qt.AlignCenter)
            min = int(self.musicBox.musicList[rowNum].duration / 60)
            sec = self.musicBox.musicList[rowNum].duration - 60 * min
            durationItem = QStandardItem("{0}分{1}秒".format(min, int(sec)))
            self.model.setItem(rowNum, 3, durationItem)
            durationItem.setTextAlignment(Qt.AlignCenter)
            sizeItem = QStandardItem("{0:.1f}".format(self.musicBox.musicList[rowNum].size / 1024 / 1024))
            self.model.setItem(rowNum, 4, sizeItem)
            sizeItem.setTextAlignment(Qt.AlignCenter)
        vBoxLayout.addWidget(self.tableView)
        self.setLayout(vBoxLayout)

    def initEvent(self):
        self.btn_selectDir.pressed.connect(self.selectDir)
        self.tableView.doubleClicked.connect(self.onDoubleClickRow)
        self.musicBox.currentMusicIndexChanged.connect(self.onMusicChange)

    def selectDir(self):
        print(QFileDialog.getExistingDirectory)
        dir = QFileDialog.getExistingDirectory(self, "选择mp3文件夹", "/")
        if len(dir) != 0:
            self.musicBox.updateMusicList(dir)
        else:
            print("未选择目录")

    def onDoubleClickRow(self, index):
        rowIndex = index.row()
        self.musicBox.play(rowIndex)

    def onMusicChange(self, index):
        print("音乐改变{}".format(index))
        self.tableView.selectRow(index)
