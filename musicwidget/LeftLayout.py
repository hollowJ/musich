from PyQt5.QtWidgets import QWidget, QVBoxLayout, QPushButton


class LeftLayout(QWidget):
    def __init__(self):
        super(LeftLayout,self).__init__()
        v = QVBoxLayout()
        v.addWidget(QPushButton("本地音乐"))
        v.addWidget(QPushButton("网络音乐"))
        self.setLayout(v)