from PyQt5.QtCore import Qt
from PyQt5.QtMultimedia import QMediaPlayer, QMediaPlaylist
from PyQt5.QtWidgets import QWidget, QHBoxLayout, QPushButton, QLabel, QSlider


class BottomLayout(QWidget):
    musicBox = None
    btn_previous = None
    totalSecond = 0

    def __init__(self, musicBox):
        self.musicBox = musicBox
        super(BottomLayout, self).__init__()
        self.initUI()
        self.initData()
        self.initEvent()

    def initUI(self):
        self.container = QHBoxLayout()
        self.btn_previous = QPushButton("上一曲")
        self.btn_play = QPushButton("播放")
        self.btn_next = QPushButton("下一曲")
        self.container.addWidget(self.btn_previous)
        self.container.addWidget(self.btn_play)
        self.container.addWidget(self.btn_next)
        self.lbl_curTime = QLabel("当前播放时间")
        self.container.addWidget(self.lbl_curTime)
        self.slide_progress = QSlider(Qt.Horizontal)
        self.container.addWidget(self.slide_progress)
        self.lbl_totalTime = QLabel("总共时间")
        self.container.addWidget(self.lbl_totalTime)
        self.lbl_VolumnStatus = QLabel("音量")

        self.container.addWidget(self.lbl_VolumnStatus)
        self.slide_volumn = QSlider(Qt.Horizontal)
        self.slide_volumn.setMinimum(0)
        self.slide_volumn.setMaximum(100)
        self.container.addWidget(self.slide_volumn)
        self.btn_loop = QPushButton("循环")
        self.container.addWidget(self.btn_loop)
        self.btn_lrc = QPushButton("歌词")
        self.container.addWidget(self.btn_lrc)
        self.setLayout(self.container)

    def initEvent(self):
        self.btn_previous.clicked.connect(self.musicBox.previous)
        self.btn_next.clicked.connect(self.musicBox.next)
        self.musicBox.playerStateChanged.connect(self.onPlayerState)
        self.btn_play.clicked.connect(self.musicBox.playOrPause)
        self.musicBox.currentMusicInfoChanged.connect(self.onMusicInfoChanged)
        self.musicBox.currentMusicDurationChanged.connect(self.onMusicDurationChanged)
        self.musicBox.playerVolumnChanged.connect(self.onPlayerVolumnChanged)
        self.slide_volumn.valueChanged.connect(self.musicBox.onPlayerVolumnChange)
        self.musicBox.playerBackModeChanged.connect(self.onPlayerBackModeChanged)
        self.btn_loop.clicked.connect(self.onPlayModeChanged)

    def initData(self):
        self.onPlayerVolumnChanged(self.musicBox.getPlayerVolumn())

    def onPlayerState(self, arg):
        if arg == QMediaPlayer.PlayingState:
            self.btn_play.setText("播放")
        elif arg == QMediaPlayer.PausedState:
            self.btn_play.setText("暂停")

    def onMusicInfoChanged(self, music):
        min = int(music.duration / 60)
        sec = music.duration - 60 * min
        self.lbl_totalTime.setText("{0}分{1}秒".format(min, int(sec)))
        self.totalSecond = music.duration / 1000
        self.slide_progress.setMaximum(int(music.duration))
        self.slide_progress.setMinimum(0)

    def onMusicDurationChanged(self, second):
        min = int(second / 1000 / 60)
        sec = second / 1000 - 60 * min
        self.lbl_curTime.setText("{0}分{1}秒".format(min, int(sec)))
        self.slide_progress.setValue(int(second / 1000))

    def onPlayerVolumnChanged(self, volumn):
        self.slide_volumn.setValue(volumn)

    def onPlayerBackModeChanged(self, mode):
        if mode == QMediaPlaylist.CurrentItemOnce:
            self.btn_loop.setText("播放一次")
        elif mode == QMediaPlaylist.CurrentItemInLoop:
            self.btn_loop.setText("单曲循环")
        elif mode == QMediaPlaylist.Sequential:
            self.btn_loop.setText("列表播放")
        elif mode == QMediaPlaylist.Loop:
            self.btn_loop.setText("列表循环")
        elif mode == QMediaPlaylist.Random:
            self.btn_loop.setText("随机")

    def onPlayModeChanged(self):
        mode = self.musicBox.getPlayerLoop()
        self.musicBox.setPlayBackMode((mode + 1) % 5)
