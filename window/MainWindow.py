from PyQt5.QtWidgets import QMainWindow, QStackedWidget, QPushButton, QVBoxLayout, QHBoxLayout, QWidget, QApplication

from datamodel.musicbox import MusicBox
from musicwidget.BottomLayout import BottomLayout
from musicwidget.LeftLayout import LeftLayout
from musicwidget.widget import MusicListWidget


class MainWindow(QMainWindow):
    musicBox=None
    def __init__(self, *argv):
        super(MainWindow, self).__init__()
        self.musicBox=MusicBox()
        self.initUI()
        self.resize(1000, 630)
        self.setWindowTitle("网易云音乐")
        desktopWidget=QApplication.desktop()
        self.move((desktopWidget.width()-self.width())/2,(desktopWidget.height()-self.height()-50)/2)

    def initUI(self):
        widget = QWidget()
        v1 = QVBoxLayout()
        v1.addWidget(QPushButton("标题"))
        h1 = QHBoxLayout()
        v1.addLayout(h1)
        h2 = QHBoxLayout()
        leftLayout = LeftLayout()
        h2.addWidget(leftLayout)
        contentView = QStackedWidget(self)
        contentView.addWidget(MusicListWidget(self.musicBox))
        h2.addWidget(contentView)
        v1.addLayout(h2)
        v1.addWidget(BottomLayout(self.musicBox))
        widget.setLayout(v1)
        self.setCentralWidget(widget)
